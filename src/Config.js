import device;

var BG_WIDTH                    = 576;
var BG_HEIGHT                   = 1024;
var BLOCK_WIDTH                 = 185;
var BLOCK_HEIGHT                = 500;
var BLOCK_OFFSETX               = 13;
var BLOCK_OFFSETY               = 15;
var BLOCK_PADDING_WIDTH         = 27;
var BLOCK_PADDING_HEIGHT        = 15;
var BLOCK_SPEED = -0.25;
// var BLOCK_SPEED = -0.1;
var PLAYER_VELOCITY_Y = -0.5;
var DISTANCE_BETWEEN_2_BLOCK    = 50;
var PLAYER_SIZE                 = 50;
var CIRCLE_SIZE                 = 70;
exports = {
  block_speed: BLOCK_SPEED,
  player_vy: PLAYER_VELOCITY_Y,
  circle_size: CIRCLE_SIZE,
  player_size: PLAYER_SIZE,
  circles: [
    {
      id: "blue",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/blue_circle.png"
    },
    {
      id: "brown",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/brown_circle.png"
    },
    {
      id: "green",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/green_circle.png"
    },
    {
      id: "grey",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/grey_circle.png"
    },
    {
      id: "gull_grey",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/gull_grey_circle.png"
    },
    {
      id: "orange",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/orange_circle.png"
    },
    {
      id: "pink",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/pink_circle.png"
    },
    {
      id: "purple",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/purple_circle.png"
    },
    {
      id: "red",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/red_circle.png"
    },
    {
      id: "turquoise",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/turquoise_circle.png"
    },
    {
      id: "white",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/white_circle.png"
    },
    {
      id: "yellow",
      x: 100,
      y: 100,
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      zIndex: 2,
      image: "resources/images/yellow_circle.png"
    },
  ],
  blocks: [
    //0
    {
      id: "blue",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/blue_block.png"
      }
    },
    //1
    {
      id: "brown",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/brown_block.png"
      }
    },
    //2
    {
      id: "green",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/green_block.png"
      }
    },
    //3
    {
      id: "grey",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/grey_block.png"
      }
    },
    //4
    {
      id: "gull_grey",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/gull_grey_block.png"
      }
    },
    //5
    {
      id: "orange",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/orange_block.png"
      }
    },
    //6
    {
      id: "pink",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/pink_block.png"
      }
    },
    //7
    {
      id: "purple",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/purple_block.png"
      }
    },
    //8
    {
      id: "red",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/red_block.png"
      }
    },
    //9
    {
      id: "turquoise",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/turquoise_block.png"
      }
    },
    //10
    {
      id: "white",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/white_block.png"
      }
    },
    //11
    {
      id: "yellow",
      x: 0,
      y: 700,
      vx: BLOCK_SPEED,
      vy: 0,
      hitOpts: {
        offsetX: BLOCK_OFFSETX,
        offsetY: BLOCK_OFFSETY,
        width: BLOCK_WIDTH - BLOCK_PADDING_WIDTH,
        height: BLOCK_HEIGHT - BLOCK_PADDING_HEIGHT,
      },
      viewOpts: {
        width: BLOCK_WIDTH,
        height: BLOCK_HEIGHT,
        image: "resources/images/yellow_block.png"
      }
    },
  ],
  player: {
    x: 200,
    // y: 668,
    y: 500,
    vx: 0,
    vy: 0.5,
    hitOpts: {
      width: PLAYER_SIZE,
      height: PLAYER_SIZE
    },
    viewOpts: {
      width: PLAYER_SIZE,
      height: PLAYER_SIZE,
      r: 0,
      zIndex: 2,
      url: "resources/images/player",
      defaultAnimation: "idle",
      loop: false
    }
  },
  player_talk: [
    "OK slick, that's your small mistake!\n Now go get some Good!",
    "Let's string this noodle!",
    "Match in the gas tank",
    "Stomp the milk box!",
    "Rub some durt on it.",
    "Bomb goes the dynamite!",
    "Take it home coach",
    "Like the wind blows",
    "Make it happen Captain",
    "The sweet smell",
    "You got the tools",
    "Light that candle",
    "Call me wonder Muffin",
    "Saddle up chief",
    "It's the fish flakes"
  ]
};