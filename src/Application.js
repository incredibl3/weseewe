import device;
import animate;
import AudioManager;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import ui.widget.GridView as GridView;

import src.Config as config;
import math.util as util;
import math.array as array;
import entities.Entity as Entity;
import entities.EntityPool as EntityPool;
import ui.ViewPool as ViewPool;
import effects;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

var app;
var totalBlockPassed;
var SHOW_HIT_BOUNDS = false;

rollFloat = function(min, max) {
  return min + Math.random() * (max - min);
};

exports = Class(GC.Application, function () {


  this.initUI = function () {

    app = this;

    //scale screen
    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    this.background = new View({
      parent: this.view,
      x: 0,
      y: -50,
      width: BG_WIDTH,
      height: 1074,
      backgroundColor: "#9ED1CE"
    });
    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        // This index is the name of the file,
        // the extension is depending on the system.
        // If you run this demo in Chromium the file
        // will be "resources/audio/background.ogg"
        // else the file will be "resources/audio/background.mp3"
        background: {
          volume: 0.8,
          loop: true,
          background: true
        },
        jumpA: {
          volume: 1,
          loop: false
        },
        jumpB: {
          volume: 1,
          loop: false
        },
        color: {
          volume: 1,
          loop: false
        },
        pop: {
          volume: 1,
          loop: false
        }
      }
    });
    this._sound.play("background", {loop: true});

    //Init Entities
    this.player = new Players({ parent: this.view });
    this.blocks = new Blocks({ parent: this.view });

    //Handle touch event
    this.view.onInputSelect = function(){
      app.player.onInputSelect();
    };

    //cloud view pool
    this.maxCloud = 0;
    this.cloudViewPool = new ViewPool({
      ctor: ImageView,
      initCount: 3,
      initOpts: {
        superview: this.view,
        width: 200,
        height: 200,
        image: "resources/images/cloud.png"
      }
    });

    //init score, counted by target color
    this.score = 0;
    this.bestScore = 0;

    //play button
    this.playBtn = new ImageView({
      parent: this.view,
      layout: "box",
      centerX: true,
      centerY: true,
      width: 250,
      height: 126,
      image: "resources/images/play.png"
    });

    //play button event
    this.playBtn.onInputSelect = bind(this, function(){
      this.startGame = true;
      this.playBtn.hide();
      this.welcomeText();
    });

    //Game over view
    this.goView = new View({
      parent: this.view,
      x: - (BG_WIDTH * 2),
      y: 0,
      zIndex: 2,
      width: BG_WIDTH * 2,
      height: BG_HEIGHT,
      backgroundColor: "#a7afb2"
    });

    this.reset();
  };

  this.welcomeText = function(){
    this.dialog = new ImageView({
      parent: this.view,
      x: 190,
      y: 595,
      width: 0,
      height: 0,
      image: "resources/images/dialog.png"
    });

    this.playerGapDialog = 75;//670 - 595;

    this.welcomeText = new TextView({
      parent: this.dialog,
      layout: "box",
      y: 10,
      width: 300,
      height: 40,
      text: choice(config.player_talk),
      size: 20,
      horizontalAlign: "center",
      autoSize: false,
      autoFontSize: true,
      fontWeight: "bold",
      color: "grey",
      wrap: true
    });
    this.welcomeText.hide();

    if(this.bestScore < this.score){
      this.bestScore = this.score;
      this.welcomeText.setText(this.bestScore + " colors, that's your best!");
    }

    animate(this.dialog).now({width: 300, height: 80}, 300, animate.linear).then(bind(this, function(){
      app.welcomeText.show();
    }));

    setTimeout(function(){
      app.welcomeText.hide();
      animate(app.dialog).now({width: 0, height: 0, x: 230, y: 660}, 300, animate.linear);
    }, 2500);
  }

  this.launchUI = function () {

  };

  /**
   * reset
   * ~ resets all game elements for a new game
   */
  this.reset = function(data) {
    totalBlockPassed = 0;
    this.isGameOver = false;
    this.startGame = false;
    this.blocks.reset();
    this.player.reset();
    this.playBtn.show();
  };

  /**
   * tick
   * ~ called automatically by devkit for each frame
   * ~ updates all game elements by delta time, dt
   */

  this.tick = function(dt) {

    if(this.isGameOver) return;

    if(this.dialog)
    this.dialog.updateOpts({y: this.player.entities[0].y - this.playerGapDialog});

    if(this.maxCloud < 2)
    {
      var view = this.cloudViewPool.obtainView();
      this.maxCloud++;
      view.updateOpts({
        parent: this.view,
        x: util.random(BG_WIDTH, BG_WIDTH * 2),
        y: util.random(100, 450)
      });
      animate(view).now({x: -250, r: Math.random() * Math.PI}, 20000, animate.easeIn).then(bind(this, function() {
              app.cloudViewPool.releaseView(view);
              app.maxCloud--;
      }));
    }

    if(this.player.entities[0].y <= BG_HEIGHT && this.player.entities[0].x >= -config.player_size){
      this.blocks.update(dt);
      this.player.update(dt);
      this.player.onFirstPoolCollisions(this.blocks, this.onBlockHit, this);
    }else if(this.player.entities[0].y > BG_HEIGHT){
      this.isGameOver = true;
      this.gameOver(true);
    }else if(this.player.entities[0].x < -config.player_size){
      this.isGameOver = true;
      this.gameOver(false);
    }
  }

  this.gameOver = function(isBottomDead){
    this._sound.play("pop", {loop: false});
    effects.hover(app.view, {
      loop: false,
      duration: 500,
      scale: 10
    });

    var scrap_paper_arr = [
      "resources/images/scrap_paper_blue.png",
      "resources/images/scrap_paper_brown.png",
      "resources/images/scrap_paper_green.png",
      "resources/images/scrap_paper_grey.png",
      "resources/images/scrap_paper_gull_grey.png",
      "resources/images/scrap_paper_orange.png",
      "resources/images/scrap_paper_pink.png",
      "resources/images/scrap_paper_purple.png",
      "resources/images/scrap_paper_red.png",
      "resources/images/scrap_paper_turquoise.png",
      "resources/images/scrap_paper_white.png",
      "resources/images/scrap_paper_yellow.png"
    ]

    this.shredded_paper = new View({
      parent: this.view,
      x: 250,
      y: 1200,
      width: 25,
      height: 175,
      zIndex: 2
    });

    if(!isBottomDead)
      this.shredded_paper.updateOpts({x: -100, y: 750});

    for(var i = 0; i <=20; i++){
      var tmp = new ImageView({
        parent: this.shredded_paper,
        x: util.random(0, 40),
        y: util.random(0, 150),
        r: Math.random(- Math.PI, Math.PI),
        width: 18,
        height: 8,
        image: choice(scrap_paper_arr),
      });
    }

    animate(this.shredded_paper).now({x: 200, y: 400}, 150, animate.linear).then(bind(this, function(){
        effects.firework(app.shredded_paper, {
        duration: 3500,
        follow: false,
        images:scrap_paper_arr
        });
        app.shredded_paper.removeFromSuperview();
    }));

    setTimeout(function() {
      animate(app.goView).now({x: BG_WIDTH * 2}, 500, animate.linear).then(bind(this, function(){
        app.goView.updateOpts({x: -(BG_WIDTH*2)});
      }));
      app.reset();
    }, 3000);
  }

  this.onBlockHit = function(player, block){
    if(player){
      if(!player.isStanding){
        // console.log("player hit block");
        if(player.y < block.y){
          player.y = block.y - 30;
          player.vy = 0;
          player.ay = 0;
          player.jumpingLevel = 0;
          player.isJumping = false;
          player.isStanding = true;
        }else if(player.y > block.y && player.isJumping == false){
          player.vx = config.block_speed;
          player.blockedByHighBlock = true;
        }
      }
    }

  }
  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});

//choose random value in array
choice = function (arr) {
  return arr[arr.length * Math.random() | 0];
}

/**
 * Obstacle Class
 * ~ an individual Obstacle
 */
var Block = Class(Entity, function() {
  var sup = Entity.prototype;
  this.name = "Block";

  this.update = function(dt) {
    if(this.view.x + 185 < 0)
    {
      if(app.startGame == true){
        totalBlockPassed++;
      }
      app.blocks.spawnCount--;
      this.destroy();
    }
    sup.update.call(this, dt);
  };
});

/**
 * Blocks Class
 * ~ a collection of Blocks
 */
var Blocks = Class(EntityPool, function() {
  var sup = EntityPool.prototype;

  this.init = function(opts) {
    opts.ctor = Block;
    sup.init.call(this, opts);
  };

  this.reset = function() {
    sup.reset.call(this);
    this.beginGame = true;
    this.activeColorCount = 1;
    this.spawnCount = 0;
    this.activeColorArray = [];
    this.nonActiveColorArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    this.activeColorArray = array.shuffle(this.nonActiveColorArray);

    if(this.lastBlock){
      this.lastBlock.view.x = -153;
      this.lastBlock.view.y = 700;
    }

    this.currentActiveColorArray = [];
    this.currentActiveColorArray[0] = this.activeColorArray[0];

    console.log("First color: " + this.activeColorArray[0]);

    this.removeNonActiveColor(this.currentActiveColorArray[0]);

    this.nonActiveColorStandTogether = 0;

    if(this.circlesArray && this.circlesArray.length > 0)
      for(var i = 0; i < this.circlesArray.length; i++)
        this.circlesArray[i].removeFromSuperview();
    this.circlesArray = [];
    this.circlesArray[0] = new ImageView(merge({parent: app.view,
      layout: 'box',
      centerX: true,
      y: 100
   }, config.circles[this.currentActiveColorArray[0]]));
  };

  this.removeNonActiveColor = function(val){
    for(var i = 0; i < this.nonActiveColorArray.length; i++){
      if(this.nonActiveColorArray[i] == val){
        this.nonActiveColorArray.splice(i, 1);
      }
    }

    // for(var i = 0; i < this.nonActiveColorArray.length; i++){
    //   console.log("hungnt nonActiveColorArray: " + this.nonActiveColorArray[i]);
    // }
  }

  this.update = function(dt) {
    sup.update.call(this, dt);

    if(app.startGame == false && this.spawnCount < 6){ //trail play generate same blocks same height, wait until player press "Play" button
      this.spawnBlock();
      this.spawnCount++;
    }else if(app.startGame == true && this.beginGame == true){ // begin game generate 7 block same height
      this.spawnBlock();
      this.spawnCount++;
      if(this.spawnCount == 7){
        this.beginGame = false;
      }
    }else if(this.spawnCount < 6){ //random block height
      this.spawnBlock();
      this.spawnCount++;
    }

    /*
    10 same color blocks passed activeColorCount = 1
    15 color blocks passed activeColorCount = 2
    30 color blocks passed activeColorCount = 3
    45 color blocks passed activeColorCount = 4
    60 color blocks passed activeColorCount = 5
    75 color blocks passed activeColorCount = 6
    90 color blocks passed activeColorCount = 7
    105 color blocks passed activeColorCount = 8
    120 color blocks passed activeColorCount = 9
    135 color blocks passed activeColorCount = 10
    */


  };

  this.updateTargetColor = function(activeColorCount){
    app._sound.play("color");
    this.score = activeColorCount;
    this.currentActiveColorArray[activeColorCount - 1] = this.activeColorArray[0];
    this.removeNonActiveColor(this.currentActiveColorArray[activeColorCount - 1]);

    // for(var i = 0; i < activeColorCount; i++){
    //   console.log("hungnt target color: " + this.currentActiveColorArray[i]);
    // }

    if(activeColorCount <= 5)
    {
      for(var i = 0; i < activeColorCount - 1; i++){
        animate(this.circlesArray[i]).now({x: this.circlesArray[i].style.x - 40 });
      }

      this.circlesArray[activeColorCount - 1] = new ImageView(merge({parent: app.view,
          layout: 'box',
          x: this.circlesArray[activeColorCount - 2].style.x + 45,
          y: 100
      }, config.circles[this.currentActiveColorArray[activeColorCount - 1]]));
    }else if(activeColorCount >= 6){
      this.circlesArray[activeColorCount - 1] = new ImageView(merge({parent: app.view,
          layout: 'box',
          x: 93 + (activeColorCount - 6) * 85,
          y: 200
      }, config.circles[this.currentActiveColorArray[activeColorCount - 1]]));
    }
  }

  this.spawnBlock = function() {

    if(totalBlockPassed == 15 || totalBlockPassed == 30 || totalBlockPassed == 45 || totalBlockPassed == 60 || totalBlockPassed == 75 ||
      totalBlockPassed == 90 || totalBlockPassed == 105 || totalBlockPassed == 120 || totalBlockPassed == 135){
    // if(totalBlockPassed == 10 || totalBlockPassed == 15 || totalBlockPassed == 20 || totalBlockPassed == 25 || totalBlockPassed == 30 ||
    // totalBlockPassed == 35 || totalBlockPassed == 40 || totalBlockPassed == 45 || totalBlockPassed == 50){
      //this.activeColorCount = parseInt(totalBlockPassed / 15) + 1;
      this.activeColorCount++;
      this.updateTargetColor(this.activeColorCount);
    }

    var type;
    var removeHitOpts = false;

    if(this.beginGame) // only generate 1 color
      type = config.blocks[this.currentActiveColorArray[0]];
    else{
      //activeColor is the one player can stand on
      //the activeColor has higher chance to spawn (85%)
      var rand = Math.random();
      //only allow spawn max 2 blocks in a row are non-active color
      if(this.nonActiveColorStandTogether >= 2){
        this.nonActiveColorStandTogether = 0;
        type = config.blocks[choice(this.currentActiveColorArray)];
      }else if(rand < 0.70){ // spawn active color
        this.nonActiveColorStandTogether = 0;
        type = config.blocks[choice(this.currentActiveColorArray)];
      }
      else{
        var rand = choice(this.nonActiveColorArray);
        console.log("hungnt rand: " + rand);
        this.nonActiveColorStandTogether++;
        type = config.blocks[rand];
        removeHitOpts = true;
      }
    }

    if(removeHitOpts){
      type.hitOpts.offsetY = 1200; //cheat move hitBound out of screen because even set width & height = 0, block still collide with player
      type.hitOpts.width = 0;
      type.hitOpts.height = 0;
      // console.log("remove hit opts: " + type.id);
    }else{
      type.hitOpts.offsetY = 15; //cheat move hitBound out of screen because even set width & height = 0, block still collide with player
      type.hitOpts.width = 158;
      type.hitOpts.height = 485;
    }

    if(this.lastBlock)
      type.x = this.lastBlock.view.x + 153;

    if(!this.beginGame)
      type.y = rollFloat(650, 800); //begin game, some block has same height

    if(this.lastBlock && Math.abs(this.lastBlock.view.y - type.y) < 10) //if this block.y almost same with lastBlock.y - > make block.y = lastBlock.y  // easier for player
      type.y = this.lastBlock.view.y;

    var block = this.obtain(type);
    this.lastBlock = block;

    SHOW_HIT_BOUNDS && block.view.showHitBounds();
  };

});

/**
 * Player Class
 */
var Player = Class(Entity, function() {
  var sup = Entity.prototype;
  this.name = "Player";
});


var Players = Class(EntityPool, function() {
  var sup = EntityPool.prototype;
  this.name = "Players";
  var _player;

  this.init = function(opts) {
    opts.ctor = Entity;
    sup.init.call(this, opts);
  };

  // This Pool is special, we don't destroy Player when reset the game
  // But we create them :)
  this.reset = function() {
    sup.reset.call(this);
    _player = this.obtain(config.player);
    _player.jumpingLevel = 0;
    _player.isJumping = false;
    _player.isStanding = false;
    //blink animation
    setInterval(function () {
      _player.view.startAnimation("blink", {
        loop: false,
        callback: function () {
          _player.view.startAnimation("idle", {loop: true});
        }.bind(this)
      });
    }, 3000);

    SHOW_HIT_BOUNDS && _player.view.showHitBounds();
  };

  this.update = function(dt) {
      //player not collide with any block -> falling
      if(app.blocks.getFirstCollidingEntity(_player) == null && _player.jumpingLevel == 0){
        // console.log("hungnt update player");
        _player.vy = -config.player_vy;
        _player.isStanding = false;
      }

      //if player blocked by high block -> stay there
      var hits = app.blocks.getAllCollidingEntities(_player);
      if(hits.length >= 2 && hits[0].y != hits[1].y) //if two blocks have same high (y1 = y2) player will not blocked
      {
        _player.vx = config.block_speed;
        _player.blockedByHighBlock = true;
      }
      else
        _player.vx = 0;

      //when blocked and player jump
      if((_player.jumpingLevel >= 1 && hits.length >= 2) || _player.blockedByHighBlock == true){
        _player.vx = config.block_speed;
      }

      //standing on block
      // if(hits.length == 1 && !_player.isJumping){
      //   if(hits[0].x <= _player.x){
      //     _player.vy = 0;
      //     _player.ay = 0;
      //     _player.jumpingLevel = 0;
      //   }
      // }

      if(hits.length == 0)  _player.blockedByHighBlock = false;
    sup.update.call(this, dt);
  }

  this.onInputSelect = function() {
    // console.log("hungnt Player onInputSelect");

    if(_player.jumpingLevel == 2) return;  //only double jump

    _player.jumpingLevel++;
    _player.vy = config.player_vy;
    _player.isJumping = true;
    _player.isStanding = false;

    animate(app.view).now({y: 10}, 250, animate.linear).then(bind(this, function(){
      animate(app.view).now({y: 0}, 250, animate.linear);
    }));

    app._sound.play("jumpA");

    if(_player.jumpingLevel == 2)
      animate(_player.view).clear();
    animate(_player.view).now({r: Math.PI * 2}, 800, animate.linear).then(bind(this, function(){
      _player.view.updateOpts({r: 0});
    }));

    setTimeout(function() {
      _player.vy = 0;
      _player.ay = 0.001;
      _player.isJumping = false;
    }, 350);

  };
});